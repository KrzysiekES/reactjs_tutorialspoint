# README #

### ReactJS tutorialspoint ###

*1.ReactJS - Environment Setup
*2.ReactJS - JSX
*3.ReactJS - Components
*4.ReactJS - State
*5.ReactJS - Props Overview
*6.ReactJS - Props Validation
*7.ReactJS - Component API
*8.ReactJS - Component Life Cycle
*9.ReactJS - Forms
*10.ReactJS - Events
*11.ReactJS - Refs
*12.ReactJS - Keys
*13.ReactJS - Router
--------------------------------
Preparing to run project
Babel
### npm install -g babel
### npm install -g babel-cli

Webpack
npm install webpack --save
npm install webpack-dev-server --save

React 
npm install react --save
npm install react-dom --save

babel plugins
npm install babel-core
npm install babel-loader
npm install babel-preset-react
npm install babel-preset-es2015